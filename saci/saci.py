#!/usr/bin/env python3

"""
Saci a simple arXiv command-line interface

Copyright © 2020-2022 Igor Benek-Lins <physics@ibeneklins.com>.

Notes
=====

We use order=-submitted_date when searching because arXiv's default
order=-announced_date_first may get us old results [citation needed].

Licence
=======
For the licence, see the LICENCE file.

"""

import os
from datetime import datetime, timedelta
import urllib.error
import urllib.request
import warnings
import html
import argparse
import termcolor as termcolor
from ansi2html import Ansi2HTMLConverter

colored = termcolor.colored

RESULTS_PER_PAGE = 25
ARXIV_RESULT_TAG = '<li class="arxiv-result">'
AUTHOR_TAG = '<a href="/search/?searchtype=author'
TITLE_TAG = '<p class="title is-5 mathjax">'
ABSTRACT_TAG = '<span class="abstract-full has-text-grey-dark mathjax"'
DATE_TAG = '<p class="is-size-7"><span class="has-text-black-bis has-text-weight-semibold">Submitted</span>'
USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0"

SEARCH_HIT_TAG_BEGIN = '<span class="search-hit mathjax">'
SEARCH_HIT_TAG_END = "</span>"
KEYWORD_HIT_BEGIN, KEYWORD_HIT_END = colored(
    "HIT", 'yellow', attrs=['bold', 'underline']).split('HIT')

def from_date_parser(date_str):
    try:
        date = int(date_str)
    except:
        try:
            date = datetime.strptime(date_str, '%Y-%m-%d')
        except:
            raise ValueError(f"Invalid date {date_str}. It should be YYYY-MM-DD"
                             " or N, with N a positive integer.")

    if isinstance(date, int):
        date_shift = date
        assert date_shift >= 0, "The date shift must be non-negative."
        date = datetime.now() - timedelta(days=date_shift)
    if isinstance(date, datetime):
        date = date.strftime("%Y-%m-%d")

    return date

def get_authors(lines, i):
    """
    Extract the authors.
    """

    authors = []
    while True:
        if not lines[i].startswith(AUTHOR_TAG):
            break
        idx = lines[i].find('>')
        if lines[i].endswith(','):
            authors.append(lines[i][idx + 1: -5])
        else:
            authors.append(lines[i][idx + 1: -4])
        i += 1
    return authors, i


def get_next_result(lines, start):
    """
    Extract paper from the XML file obtained from arXiv search.

    Each paper is a dict that contains:
    + 'title': str
    + 'pdf_link': str
    + 'url': str
    + 'authors': list
    + 'authors_short': str
    + 'abstract': str
    """

    result = {}

    # These values may change when arXiv updates their webpages format.
    page_and_id_line = 2
    files_line = 3
    doi_flag_line = 10
    doi_line = doi_flag_line + 1
    authorship_line = 5

    line_with_page_and_id = lines[start + page_and_id_line].split('"')
    line_with_files = lines[start + files_line].split('"')
    possible_line_with_doi_flag = lines[start + doi_flag_line]
    doi_flag = "doi" in possible_line_with_doi_flag

    result['url'] = line_with_page_and_id[3]
    result['arxiv'] = line_with_page_and_id[4].replace('</a>', '').replace('>', '')

    for item in line_with_files:
        if "http" in item:
            if "pdf" in item:
                result["pdf"] = item + '.pdf'
            if "ps" in item:
                result["ps"] = item
            if "others" in item:
                result["other_formats"] = item
                result["source"] = item.replace("/format/", "/e-print/")

    if doi_flag:
        line_with_doi = lines[start + doi_line].split('"')
        for item in line_with_doi:
            if "doi" in item:
                result['doi_url'] = item
                result['doi'] = item.replace("https://doi.org/", '')

    while lines[start].strip() != TITLE_TAG:
        start += 1

    title = lines[start + 1].strip()
    # Remove highlighting of the keywords found.
    title = title.replace(SEARCH_HIT_TAG_BEGIN, '')
    title = title.replace(SEARCH_HIT_TAG_END, '')
    result['title'] = title

    authors, start = get_authors(
        lines, start + authorship_line)

    while not lines[start].strip().startswith(ABSTRACT_TAG):
        start += 1
    abstract = lines[start + 1]
    # Remove highlighting of the keywords found.
    # abstract = abstract.replace(SEARCH_HIT_TAG_BEGIN, '')
    # abstract = abstract.replace(SEARCH_HIT_TAG_END, '')
    # Add terminal highlighting on the found keywords.
    abstract = abstract.replace(SEARCH_HIT_TAG_BEGIN, KEYWORD_HIT_BEGIN)
    abstract = abstract.replace(SEARCH_HIT_TAG_END, KEYWORD_HIT_END)
    result['abstract'] = abstract

    result['authors'] = authors
    if len(authors) > 1:
        result['authors_short'] = authors[0] + " et al."
    else:
        result['authors_short'] = authors[0]

    while not lines[start].strip().startswith(DATE_TAG):
        start += 1

    idx = lines[start].find('</span> ')
    end = lines[start][idx:].find(';')

    result['non_iso_date'] = lines[start][idx + 8: idx + end]
    # There is no reason to stop the routine just because the date
    # is badly formatted.
    try:
        iso_date = datetime.strptime(result['non_iso_date'], '%d %B, %Y')
        result['iso_date'] = iso_date.strftime("%Y-%m-%d")
    except TypeError:
        result['iso_date'] = result['non_iso_date']

    return result, start


def clean_empty_lines(lines):
    cleaned = []
    for line in lines:
        line = line.strip()
        if line:
            cleaned.append(line)
    return cleaned


def get_report(paper):
    """
    Construct the output for each article.
    """

    # Title of article.
    title = f"{html.unescape(paper['title'])}"
    title_colored = colored(title, 'yellow', attrs=['bold'])

    # Authorship and date.
    subtitle = f"{paper['authors_short']} on {paper['iso_date']}"
    subtitle_colored = colored(subtitle, 'green')

    # Headline.
    headline = f"\n# {title}\n- {subtitle})"
    headline_colored = f"\n# {title_colored}\n- {subtitle_colored}"

    # Abstract.
    abstract = html.unescape(paper['abstract'])
    abstract_colored = colored(abstract)

    # IDs (arXiv and DOI).
    arxiv = html.unescape(paper['arxiv'])
    identification = f"{arxiv}"
    if "doi" in paper:
        identification += f", doi:{paper['doi']}"
    identification_colored = colored(identification, 'cyan')

    # arXiv URL to the article.
    url = paper['url']
    url_colored = colored(url, 'blue')

    report = (headline_colored + f"\n{abstract_colored}"
              + f"\n· Identification: {identification_colored}"
              + f"\n· Hyperlink: {url_colored}\n")

    return report


def txt2reports(txt, num_to_show):
    """
    Search for articles.
    """

    found = False
    num_found = 0
    lines = txt.split('\n')
    lines = clean_empty_lines(lines)
    unshown = []

    for i, current_line in enumerate(lines):
        if num_to_show <= 0:
            return unshown, num_to_show, found

        line = current_line.strip()
        if len(line) == 0:
            continue
        if line == ARXIV_RESULT_TAG:
            found = True
            num_found += 1
            paper, i = get_next_result(lines, i)
            report = get_report(paper)
            unshown.append(report)
        if line == '</ol>':
            # The end of the line for us.
            break
        if num_found > num_to_show:
            # Good work, people! We got more than we were asked for.
            # Let us take a break.
            break
    return unshown, found, num_found


def get_papers(keyword_orig, num_results=5):

    keyword = keyword_orig.lower()
    query_template = "https://arxiv.org/search/advanced?advanced=1&terms-0-operator=AND&terms-0-term={}&terms-0-field=all&classification-physics=y&classification-physics_archives=all&date-filter_by={}&date-year=&date-from_date={}&date-to_date={}&date-date_type=submitted_date&abstracts=show&size={}&order=-submitted_date&start={}"
    keyword_query = keyword.replace(' ', '+')
    page = 0
    per_page = RESULTS_PER_PAGE
    num_to_show = num_results
    all_unshown = []
    html_output = ""

    while num_to_show > 0:
        query = query_template.format(
            keyword_query, date_filter_by, date_from, date_to,
            str(per_page), str(per_page * page))
        if verbose:
            print(colored(f"{verbose_tag} Querying {query}", 'red'))

        req = urllib.request.Request(query, headers={'User-Agent': USER_AGENT})
        try:
            response = urllib.request.urlopen(req)

        except urllib.error.HTTPError as error:
            print(f"[error] Error {error.code} when accessing “{query}”")
            return ""

        txt = response.read().decode('utf8')
        unshown, found, num_found = txt2reports(txt, num_to_show)
        if (not found) and (not all_unshown) and num_to_show == num_results:
            print(f"We were unable to find any result with"
                  f" “{keyword_orig}” for the given parameters.")
            return ""

        if not found:
            return html_output

        if found:
            for report in unshown[:num_to_show]:
                if html_output_flag:
                    html_output += report
                else:
                    print(report)
            num_to_show -= len(unshown)

        page += 1

    return html_output


def main():
    if 'nt' in os.name:
        try:
            import win_unicode_console
            win_unicode_console.enable()
        except ImportError:
            warnings.warn(
                "On Microsoft Windows, encoding errors may arise when"
                " displaying the output. If such errors occur, install"
                " ``win_unicode_console`` using pip or use a UNIX-based"
                " operating system. Otherwise, set the output as an HTML"
                " file.")

    keyword = " ".join(args.keywords)

    output = get_papers(keyword, num_results)
    if output != "":
        html_output = Ansi2HTMLConverter(
            linkify=True, inline=True, dark_bg=False).convert(output)
        # Until https://github.com/pycontribs/ansi2html/issues/153 is
        # fixed, let us change the background colour from grey to white.
        html_output = html_output.replace(
            ".body_background { background-color: #AAAAAA; }",
            ".body_background { background-color: #FFFFFF; }")
        if html_output_flag:
            output_file = open(output_file_name, "w")
            for line in html_output.split('\n'):
                output_file.write(line)
                output_file.write('\n')
            output_file.close()


parser = argparse.ArgumentParser()
parser.add_argument("keywords",
                    type=str,
                    nargs='+',
                    help="Search terms.")
parser.add_argument("-v", "--verbose",
                    dest="verbose",
                    action="store_true",
                    default=False,
                    help="Verbosity flag. Default: False.")
parser.add_argument("-n", "--number",
                    dest="number_results",
                    type=int,
                    action="store",
                    default="5",
                    help="Number of results. Default: 5.")
parser.add_argument("-f", "--from",
                    dest="date_from",
                    type=str,
                    action="store",
                    default="",
                    help="Start date as YYYY-MM-DD or N for the previous last N days. Default: None.")
parser.add_argument("-t", "--to",
                    dest="date_to",
                    type=str, action="store",
                    default="",
                    help="End date as YYYY-MM-DD or N for the previous last N days. Default: None.")
parser.add_argument("-o", "--output",
                    dest="output_file_name",
                    type=str,
                    action="store",
                    default="",
                    help="File to store the output as an HTML file. Optional.")

args = parser.parse_args()

if args.output_file_name == "":
    html_output_flag = False
else:
    output_file_name = args.output_file_name
    html_output_flag = True

verbose = args.verbose
verbose_tag = "[verbose]"

# Dates
date_from = args.date_from
date_to = args.date_to
if args.date_from:
    date_filter_by = "date_range"
    date_from = from_date_parser(date_from)
elif args.date_to:
    date_filter_by = "date_range"
    date_to = from_date_parser(date_to)
else:
    date_filter_by = "all_dates"

# Number of results
num_results = args.number_results
assert num_results > 0, "You must choose to show a positive number of results."

if __name__ == '__main__':
    main()
