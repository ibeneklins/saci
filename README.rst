===========================================
Saci, a simple arXiv command-line interface
===========================================

Saci is a command-line interface to crawl arXiv search results and pretty-print them into the console or export them as a simple HTML file. In short, it is a simple interface to arXiv's `advanced search form <https://arxiv.org/search/advanced>`_ which can be run from the terminal.

The search terms will be searched in all fields. You can customise the number of articles in the output (it will crawl results until that number is reached), specify a date range for the search (either by giving two dates or two relative numbers of days from “today”) and also set the output to be an HTML file.

Features
========

* ✅ Run arXiv searches from the terminal.
* ✅ Coloured terminal output.
* ✅ HTML output.
* ✅ Scriptable. You can run it as a cronjob with the HTML output option.

Planned
-------

* Option to search only titles or abstracts, in addition to all fields.
* Support for recurrent searches via a configuration file with
  pre-defined search parameters.
* Option to turn on/off colouring of the terminal output.
* JSON output.

Installation
============

If you want to install Saci globally in your system, follow these steps:

1. First, clone this repository and enter the newly created folder ``saci``:

.. code-block:: bash

    $ git clone https://gitlab.com/beneklins/saci.git
    $ cd saci

2. Then, install it using ``pip``:

.. code-block:: bash

    $ pip install .

3. You should now be able to run the routine by calling ``saci`` from the command line.
   See section ``Usage`` on how to use it or just execute ``saci --help``.

Windows users
-------------

On Windows, due to encoding errors, the script may cause issues when run on the
command line. It is recommended to use `pip install win-unicode-console --upgrade`
prior to launching Saci. If you get UnicodeEncodingError, you *must* install
the above.

Other than that, you are on your own.

Usage
=====

To query for certain search terms, simply run:

.. code-block:: bash

    $ saci "[search terms]"

It will display the 5 (five) last results for that search query. Each result contains the title of the paper, the authors, published date, the full abstract, IDs (arXiv, DOI et cetera) and the link to the paper.

You can also change the number of results shown with `-n [number of results]`. For example:

.. code-block:: bash

    $ saci "superconductor raman response" -n 10

The full usage information can be found with the `--help` option::

    usage: saci.py [-h] [-v] [-n NUMBER_RESULTS] [-f DATE_FROM] [-t DATE_TO] [-o OUTPUT_FILE_NAME] keywords

    positional arguments:
    keywords              Keywords to search.

    options:
    -h, --help            show this help message and exit
    -v, --verbose         Verbosity flag. Default: False.
    -n NUMBER_RESULTS, --number NUMBER_RESULTS
                            Number of results to show. Default: 5 (five).
    -f DATE_FROM, --from DATE_FROM
                            Start date as YYYY-MM-DD or N for the previous last N days. Default: None.
    -t DATE_TO, --to DATE_TO
                            End date as YYYY-MM-DD or N for the previous last N days. Default: None.
    -o OUTPUT_FILE_NAME, --output OUTPUT_FILE_NAME
                            File to store the output as an HTML file. Optional.

Similar projects
================

- ``arxiv.py``, https://github.com/lukasschwab/arxiv.py: Python wrapper for the arXiv API.
- ``sotawhat``, https://github.com/chiphuyen/sotawhat: Crawls arXiv search results, prioritises abstracts with numbers and extract sentences that contain numbers using Natural Language Toolkit (NLTK). Saci is based on sotawhat.
- ``arxiv`` by suuuehgi, https://github.com/suuuehgi/arxiv: From lists of new submissions, generate a digest and allow the download of entries.


Licence
=======

Saci is libre software and licensed under the GPL (GNU General Public licence),
either version 3 or (at your option) any later version.
See the bundled LICENCE file for details.
